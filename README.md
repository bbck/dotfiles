# dotfiles
My personal dotfiles for both my home and work laptops.

## Installing
1. Clone into `~/Developer/dotfiles`
2. Run the `setup.sh` script (Will delete anything in it's way)
3. Add any machine speficic changes to `.localrc`

## Issues and workarounds

### How to change the default git email
Set `GIT_AUTHOR_EMAIL` and `GIT_COMMITTER_EMAIL` in `.localrc` 
