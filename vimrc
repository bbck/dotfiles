colorscheme Tomorrow-Night
syntax enable

set tabstop=2
set softtabstop=2
set expandtab

set number
set showcmd
set cursorline
filetype indent on
set lazyredraw
set showmatch

call pathogen#infect()
